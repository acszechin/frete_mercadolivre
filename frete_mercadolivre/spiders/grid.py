# coding: utf-8

import scrapy
from scrapy import Selector
from selenium import webdriver


class GridSpider(scrapy.Spider):

    name = 'grid'
    start_urls = ['https://carros.mercadolivre.com.br/acessorios/']

    def parse(self, response):
        self.driver = webdriver.Firefox()
        self.driver.get('http://www.americanas.com.br')
        print '\n----------------------------------------------------------------'
        for element in response.css('.item__info-link.item__js-link').extract():

            name = self.extract_content(element, 'span.main-title ::text')
            price_fraction = self.extract_content(element, '.item__price > .price__fraction ::text')
            price_decimal = '00' if not self.extract_content(element, '.item__price > .price__decimals ::text') else self.extract_content(element, '.item__price > .price__decimals ::text')
            price = 'R$ {},{}'.format(price_fraction, price_decimal)

            search = self.driver.find_element_by_css_selector('#h_search-input')
            search.send_keys(name)

            form = self.driver.find_element_by_css_selector("div#h_search > form")
            form.submit()

            print name, price

            raw_input('--> ')
        print '----------------------------------------------------------------\n'

    def extract_content(self, element, selector):
        value = Selector(text=element).css(selector).extract()
        if value:
            return value[0]
        return ''
